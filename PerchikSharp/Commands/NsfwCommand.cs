﻿using System;
using System.Collections.Generic;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace PerchikSharp.Commands
{
    class NsfwCommand : INativeCommand
    {
        public string Command { get { return "nsfw"; } }
        public bool Enabled { get; private set; } = false;
        
        public async void OnExecution(object sender, CommandEventArgs command)
        {
            Pieprz bot = sender as Pieprz;
            Message message = command.Message;

            if (!bot.isUserAdmin(message.Chat.Id, message.From.Id))
                return;

            await bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);

            Enabled = !Enabled;

            if (Enabled)
            {
                await bot.SendTextMessageAsync(
                                   chatId: message.Chat.Id,
                                   text: string.Format(Program.strManager["NSFW_TOGGLE_ON"], bot.MakeUserLink(message.From)),
                                   parseMode: ParseMode.Markdown);
            }else
            {
                await bot.SendTextMessageAsync(
                                   chatId: message.Chat.Id,
                                   text: string.Format(Program.strManager["NSFW_TOGGLE_OFF"], bot.MakeUserLink(message.From)),
                                   parseMode: ParseMode.Markdown);
            }
           
        }
    }
}
